Usage:
=======

.. code-block:: python

  #!/usr/bin/env python3

  from peertube.peertube import PeerTube, PeerTubeVideo

  p = PeerTube('https://peertube.example.com', 'username', 'password')
  v = PeerTubeVideo("/tmp/video.mp4")
  p.video_upload(v)
