import re
import sys
from setuptools import setup, find_packages

with open('src/peertube/_version.py') as fp:
    for line in fp:
        mo = re.match("__version__ = '(?P<version>[^']+?)'", line)
        if mo:
            __version__ = mo.group('version')
            break
    else:
        print('No version number found')
        sys.exit(1)


setup(
    name="peertube",
    version=__version__,
    description="Python bindings for Peertube API",
    long_description=open('README.rst').read(),
    maintainer="Peter Zahradnik",
    license='GPLv3',
    keywords='peertube video',
    url="https://gitlab.com/petzah/python-peertube",
    classifiers=[
        "Development Status :: 3 - Alpha",
        "License :: OSI Approved :: MIT License",
        "Programming Language :: Python :: 3",
    ],
    packages=find_packages('src'),
    package_dir={'': 'src'},
    include_package_data=True,
    install_requires=[
        'requests',
        'requests_toolbelt',
    ],
    tests_require=[
        "mock",
        "pytest",
        "vcrpy",
    ],
)
