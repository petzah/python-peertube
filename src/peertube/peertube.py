# -*- coding: utf-8 -*-

from . import USERAGENT
from requests import Session
from requests_toolbelt.multipart.encoder import MultipartEncoder
try:
    from urllib.parse import urljoin
except ImportError:
    from urlparse import urljoin
import os


def lazy_property(fn):
    attr_name = '_lazy_' + fn.__name__

    @property
    def _lazy_property(self):
        if not hasattr(self, attr_name):
            setattr(self, attr_name, fn(self))
        return getattr(self, attr_name)
    return _lazy_property


class PeerTubeSession(Session):

    def __init__(self, base_url=None, *args, **kwargs):
        super(PeerTubeSession, self).__init__(*args, **kwargs)
        self.base_url = base_url
        self.headers.update({'User-Agent': USERAGENT})

    def request(self, method, url, *args, **kwargs):
        url = urljoin(self.base_url, url)
        return super(PeerTubeSession, self).request(method, url, *args, **kwargs)


class PeerTube(object):

    _api_version = "v1"
    _api_prefix = "/api"

    def __init__(self, base_url, username, password):
        self.username = username
        self.password = password
        self.session = PeerTubeSession(base_url)
        self.session.headers.update({"Authorization": "Bearer {}"
                                     .format(self._get_token().get('access_token'))})

    def _call(self, method, url, *args, **kwargs):
        url = "{}/{}{}".format(PeerTube._api_prefix, PeerTube._api_version, url)
        response = self.session.request(method, url, *args, **kwargs)
        response.raise_for_status()
        return response.json()

    def _get_token(self):
        data = self._call("GET", "/oauth-clients/local")
        data['username'] = self.username
        data['password'] = self.password
        data['grant_type'] = "password"
        data['response_type'] = "code"
        return self._call('POST', "/users/token", data=data)

    @lazy_property
    def licences(self):
        return self._call("GET", "/videos/licences")

    @lazy_property
    def languages(self):
        return self._call("GET", "/videos/languages")

    @lazy_property
    def categories(self):
        j = self._call("GET", "/videos/categories")
        return {name: PeerTubeCategory(cid, name) for cid, name in j.items()}

    @lazy_property
    def privacies(self):
        return self._call("GET", "/videos/privacies")

    @lazy_property
    def me(self):
        return PeerTubeUser(**self._call("GET", "/users/me"))

    def video_upload(self, video, videoChannel=None):
        fields = vars(video)
        if not videoChannel:
            fields['channelId'] = str(self.me.videoChannels[0].id)
        data = MultipartEncoder(fields=fields)
        return self._call("POST", "/videos/upload", data=data, headers={'Content-Type': data.content_type})


class PeerTubeCategory(object):

    def __init__(self, cid, name):
        self.cid = cid
        self.name = name

    def __str__(self):
        return str(self.name)

    __repr__ = __str__


class PeerTubePrivacy(object):
    PUBLIC = "1"
    PRIVATE = "3"
    UNLISTED = "2"


class PeerTubeUser(object):
    def __init__(self, **kwargs):
        self.__dict__.update(kwargs)
        self.videoChannels = [PeerTubeVideoChannel(**vc) for vc in self.videoChannels]


class PeerTubeVideoChannel(object):
    def __init__(self, **kwargs):
        self.__dict__.update(kwargs)


class PeerTubeVideo(object):

    MIMESIGNATURES = [
        {'mime': 'video/mp4', 'ext': 'avi', 'sig': '52 49 46 46'},
        {'mime': 'video/mp4', 'ext': 'flv', 'sig': '46 4C 56 01'},
        {'mime': 'video/mp4', 'ext': 'mkv', 'sig': '1A 45 DF A3'},
        {'mime': 'video/mp4', 'ext': 'mp4', 'sig': '66 74 79 70'},
    ]

    def __init__(self, path, name=None, privacy=PeerTubePrivacy.PUBLIC, **kwargs):
        if not name:
            name = os.path.splitext(os.path.basename(path))[0]
        self.name = name
        self.privacy = privacy
        self.videofile = (os.path.basename(path), open(path, 'rb'), self._get_mime(path))

    def _get_mime(self, path):
        sig = " ".join(['{:02X}'.format(b) for b in bytearray(open(path, "rb").read(16))])
        for m in self.__class__.MIMESIGNATURES:
            if m.get('sig') in sig:
                return m.get('mime')
        raise ValueError('Video type not supported.')
