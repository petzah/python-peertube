# -*- coding: utf-8 -*-

# Copyright (c) 2018, Peter Zahradnik. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

"""python-peertube is a python library for interacting with PeerTube instance.
Works with Python versions from 2.7 to 3.X.
"""
from ._version import __version__


USERAGENT = "python-peertube/{}".format(__version__)
