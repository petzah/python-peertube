# -*- coding: utf-8 -*-
import unittest
import vcr
from peertube.peertube import PeerTube, PeerTubeVideo


class PeerTubeTest(unittest.TestCase):

    @vcr.use_cassette("test_vcr.yaml")
    def test_peertube_categories(self):
        p = PeerTube('http://peertube.znik.lan', 'root', 'rootroot')
        c = p.categories
        self.assertIsNotNone(c)
        self.assertIsInstance(c, dict)

    @vcr.use_cassette("test_vcr.yaml", record_mode='new_episodes')
    def test_peertube_video_upload(self):
        p = PeerTube('http://peertube.znik.lan', 'root', 'rootroot')
        v = PeerTubeVideo("tests/testdata/test.mp4")
        print(p.video_upload(v))


if __name__ == "__main__":
    unittest.main()
