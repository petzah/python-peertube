# -*- coding: utf-8 -*-
import unittest
from peertube.peertube import PeerTubeCategory
try:
    from unittest.mock import patch
except ImportError:
    from mock import patch


class PeerTubeTest(unittest.TestCase):

    @patch('peertube.peertube.PeerTube')
    def test_peertube_categories(self, MockPeerTube):
        p = MockPeerTube()
        p.categories.return_value = [
            PeerTubeCategory('1', 'Music'),
            PeerTubeCategory('2', 'Vehicles'),
        ]
        response = p.categories()
        self.assertIsNotNone(response)
        self.assertIsInstance(response, list)

    @patch('peertube.peertube.PeerTube')
    def test_peertube_licences(self, MockPeerTube):
        p = MockPeerTube()
        p.licences.return_value = {
            '1': 'Attribution',
            '2': 'Attribution - Share Alike',
            '3': 'Attribution - No Derivatives',
        }
        response = p.licences()
        self.assertIsNotNone(response)
        self.assertIsInstance(response, dict)


if __name__ == "__main__":
    unittest.main()
