#!/usr/bin/env python3

import vcr
from peertube.peertube import PeerTube, PeerTubeVideo

if __name__ == "__main__":
    with vcr.use_cassette('test_vcr.yaml'):
      p = PeerTube('http://peertube.znik.lan', 'root', 'rootroot')
      print(p.categories)
